# Runtime workflow modeling scenarios
This repository contains performed case studies in the context of the [runtime workflow modeling project](https://gitlab.gwdg.de/rwm/).
For each case study performed this repository stores the utilized OCCI model, attributes required to enhance it with platform specific characteristics, as well as video demonstration
of the case study using [SmartWYRM](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm/). Each case study got executed on In our case the case studies were performed on a private Openstack cloud (Version: Newton).
For the configuration management the [MoDMaCAO](https://github.com/occiware/MoDMaCAO) framework is used allowing to deploy applications via typicall configuraiton management scripts.
The performed case studies are separated into two groups, as described in the following.

<br></br>

## Workflow case study models and videos
In this category the enactement of runtime workflow models based on OCCI is demonstrated.
Within the [workflow case studies](./wocci) the Workflows through OCCI ([WOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.wocci/)) extension and engine is demonstrated.
In the following the case studies described in papers are listed:

- #### Journal of Software and Systems Modeling (2023): [Scientific workflow execution in the cloud using a dynamic runtime model](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/tree/master/wocci/) 

- #### Workshop on Simulation Science (2019): [Dynamic Management of Multi-Level-Simulation Workflows in the Cloud](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/tree/master/wocci/mls) 



<br></br>

## Deployment case study models and videos
In this category the provisioning and deployment of cloud application topologies using OCCI is demonstrated.
Within the [deployment case studies](./docci) the models at runtime engine for OCCI models, Deployment with OCCI ([DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci)) is demonstrated.

- #### SEAMS (2019): [OCCI-Compliant Fully Causal-Connected Architecture Runtime Models Supporting Sensor Management](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci)

<br></br>

## Case study reproduction
To reproduce the case studies the [MartServer](https://github.com/occiware/MartServer) of the [OCCIWare project](https://github.com/occiware) is required.
The MartServer serves as OCCI API which allows for the developed rwm extension to be plugged in. A preconfigured version of this server as well as a tutorial on how to 
set it uo to perform the case studies described above can be found [**here**](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/).


*Note: If no cloud environment is accessible, the results can be reproduced on a model based level. Here, the deployment and workflow enactment is performed without an actual provisioning and deployment of the cloud application topology or workflow.
Please note that for an actual reproduction of the presented scenarios a cloud environment is required.
Moreover, an infrastructure connector is required that translates incoming OCCI infrastructure requests to requests of the used cloud provider.*
