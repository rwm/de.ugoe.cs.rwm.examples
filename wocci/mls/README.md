# Scenario Description
In this scenario, a supply chain multi-level simulation is enacted in form of a workflow.
Within this scenario a decision making has to be done deciding which site within the supply chain simulation should be investigated in more detail.
Therefore, we provide two example workflow models. One in which a manual decision making is required by the user and one where it is done automatically via a deployed sensor.
Hereby, the combination of the [workflow extension and engine (WOCCI)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.wocci) with the [monitoring extension (MOCCI)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci) is demonstrated.
The workflow engine utilizes the [models at runtime engine (DOCCI)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) and is accessible via graphical interface using [SmartWYRM](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm) .

## Supply Chain Workflow (Manual)
In the video below you see a demonstration of the deployment process for the [manual supply chain workflow](mlsManual.occic).
The OCCI configuration to be deployed consists of 3 virtual machines hosting one coarse and two detailed simulation applications. 
In the video below the described workflow is enacted. Moreover, the results of the workflow are shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![MLS Manual](mlsManual.mp4)

## Supply Chain Workflow (Automatically)
In this video the [automatic supply chain workflow](mlsDecision.occic) is enacted. This workflow is similar to the one described above. However, this time the decision making is done automatically using a deployed sensor.
This sensor consists of a simple resultprovider gathering the results of the coarse task at runtime and publishes the information within the runtime workflow model.

![MLS Automatic](mlsDecision.mp4)

## Supply Chain Workflow (Looped)
In this video the [supply chain workflow is looped](mlsLoop.occic). This workflow is similar to the one manual workflow and demonstrates how a someone can interact with the simulation at runtime. The simulation continues until the while loop is set to false. Throughout the execution, the detailed simulations can be adjusted at runtime. Both interactions take place by simply adjusting the decision input of the loop or the decision node at runtime. To shorten the deployment times, this video was recorded using dummy connectors simulating the beheviour of the workflow layer.

![MLS Loop](mlsLoop.mp4)

## Configuration management scripts
Configuration management scripts are used to deploy the applications and components required by the workflow and to enact the individual tasks. The exact version of the configuration management scripts used can be found [in this revision](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/ac3e6f5ee3bb7d65984c470dfd428937f7bf4beb). The up to date version for the individual scripts are enumerated below:

* [Coarse Component](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/coarsecomp)
* [Coarse Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/coarsejob)
* [Coarse Resultprovider](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/coarseresult)
* [Picking Component](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/pickingcomp)
* [Picking Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/pickingjob)
* [QA Component](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/qacomp)
* [QA Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/qajob)
* [Simulate All Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/simalljob)

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the workflow configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.04 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |
