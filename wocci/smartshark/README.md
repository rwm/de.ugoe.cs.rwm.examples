# Scenario Description
In this scenario, a software repository mining workflow is executed. Hereby, we use the mining, and analytic process of the [SmartSHARK](https://smartshark.github.io/) framework, which is used to perform empirical studies on open source projects. The SmartSHARK framework consists of multiple components for which we generated a [domain specific extension](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.domain.shark) containing elements to deploy its applications and components to trigger its processing. The mining process is composed of three steps. First, metadata is extracted from the repository to mine and stored in a database. Next, each commit of a software project is checked out to analyze its metrics and finally store the results in a database again. Thereafter, the database needs to be cleaned up to reduce the amount of required storage. While the first and the last step of this process requires only a few computing resources, the second step requires as much computing resources as possible as it can be easily parallelized.
Within this scenario, we demonstrate the utilization of loops and their parallelilzation in the context of a runtime workflow model.
The workflow engine utilizes the [models at runtime engine (DOCCI)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) and is accessible via graphical interface using [SmartWYRM](https://gitlab.gwdg.de/rwm/smartwyrm).

## SmartShark Without Parallelization
In the video below you see a demonstration of the workflow execution for the [shark_base](shark_base.occic) workflow. The workflow is executed to gather and analyze metrics of each of the 66 commits from the [COCCI Project](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cocci).
The OCCI configuration to be deployed consists of a single virtual machines hosting each of the smartshark plugins (VCS, Meco and Meme) as well as mongoDB.
To gain knowledge about the commits for which the metrics shall be gathered, a Sensor is deployed querying the commits of the project to analyse.
In the video below the described workflow is enacted. Moreover, the results of the workflow are shortly demonstrated by plotting the gathered information using a jupiter notebook.
Please note that the provisioning and deployment times are speeded up:

![SmartSharkForEach](SmartSharkForEach(Edited).mp4)

## SmartShark With Parallelization 3
In this video the [shark_par3](shark_par3.occic) workflow is enacted. This workflow is similar to the one described above. However, this time the loop contains a parallel mixin with the replication counter set to three.
Hereby, the resources used to analyze the individual commits is tripled.

![SmartSharkForEachPar3](SmartSharkForEachPar3(Edited).mp4)

## Further SmartShark Variations
In addition to the two videos provided above, this workflow was tested with a [distributed mongodb](shark_distr_db.occic), as well as with a different parallelization numbers. Finally, we also tested the behaviour of removing the platformdependency from the loop to the Meco application using the [shark_par3_noPlat](shark_par3_noPlat) model.

## Configuration management scripts
Configuration management scripts are used to deploy the applications and components required by the workflow and to enact the individual tasks. The up to date version for the individual scripts are enumerated below:

* [VCS Shark](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/vcsshark)
* [Meme Shark](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/memeshark)
* [Meco Shark](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/mecoshark)
* [VCS Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/vcsjob)
* [Meme Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/memejob)
* [Meco Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/mecojob)
* [Mongo DB](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ogf_org/tosca/extended/tosca_nodes_mongod)
* [Mongo Query](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_ugoe_cs_rwm/domain/shark/mongoquery)

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the workflow configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.04 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |
