# Scenario Description
In this scenario, the on demand utilization of a big data framework within a workflow demonstrated. Hereby, we use the [Hadoop](https://hadoop.apache.org/) framework that can be used to perform MapReduce tasks like counting the occurences of a specific word within a set of textfiles. In this workflow, we count the occurences of words within a set of textfiles gathered from an internet archive. As a first step within the workflow, the textfiles are fetched. Next, the gathered data is stored within the hadoop distributed file system. Finally, the hadoop job gets executed performing a wordcount on the previously gathered data. Among others this use case demonstrates the utilization of a dataflow transferring files from one task to another.
The workflow engine utilizes the [models at runtime engine (DOCCI)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) and is accessible via graphical interface using [SmartWYRM](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm).

## Wordcount Workflow (Distributed Fetcher)
In the video below you see a demonstration of the workflow execution for the [wordcount workflow](wordcount_workflow_distr_fetcher) in which we utilize a virtual machine only responsible for fetching the textfiles to analyze. After the fetching task the gathered files are transferred to the next task which stores the fiels into the hdfs. Therefore, a dataflow is triggered moving the files from the fetching virtual machine to the machine hosting the hadoop master. Hereby, the hadoop cluster is already provisioned so that the task can directly distribute the data within the cluster.
The OCCI configuration to be deployed consists of a four virtual machines. One that fetches the files and three building the haddop cluster (1 master, 3 worker). 

## Wordcount Workflow (Local Fetcher)
In this video a similar [wordcount workflow](wordcount_workflow) is executed. However, this time the fetching vm is reutilized and serves as the hadoop master later in the workflow process. This way the data has not to be transferred between two hosts making the workflow more efficient. Moreover, we reduced the amount of worker nodes by one. It should be noted that the addition of worker nodes only requires to add an additional VM which hosts the worker component and is connected to the master via a componentlink.

## Video demonstration
In the video below the described workflow is enacted using a local fetcher and a hadoop cluster size of 2. Moreover, the results of the workflow are shortly demonstrated by.
Please note that the provisioning and deployment times are speeded up:

![wordcount_workflow](wordcount_workflow.mp4)

## Configuration management scripts
Configuration management scripts are used to deploy the applications and components required by the workflow and to enact the individual tasks. The up to date version for the individual scripts are enumerated below:

* [Wget Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/wget)
* [HDFS Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/hdfs)
* [WC Job](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/hadoop-wordcount)
* [Hadoop Master](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/hadoop-master)
* [Hadoop worker](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/tree/master/cm/ansible/roles/schemas_modmacao_org/usermixins/hadoop-worker)

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the workflow configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.04 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |
