# Description
In this scenario, a simple [ELK configuration](ELK.occic) is deployed.
Within this stack metrics are gathered via collectd and rsyslog and processed by the ELK stack.
To realize the deployment configuration management scripts were adapted from [here](https://github.com/openstack/heat-translator/tree/master/translator/tests/data).

## ELK stack
In the video below you see a demonstration of the deployment process for the [ELK configuration](ELK.occic).
The OCCI configuration to be deployed consists of 3 virtual machines hosting, 1 elasticsearch server, 1 logstash server, and 1 kibana server. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![ELK](ELK-edited.mp4)


## Configuration management scripts
The configuration management scripts used can be found here:

* [Kibana](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/kibana)
* [Logstash](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/logstash)
* [Elasticsearch](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/elasticsearch)

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the LAMP configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.05 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |
