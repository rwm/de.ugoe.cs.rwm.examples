# Description
In this scenario, a [Nodecellar](https://github.com/ccoenraets/nodecellar) application is deployed.
Hereby, a special TOSCA extension is created for OCCI to support the management of LAMP stacks via [MoDMaCAO](https://github.com/occiware/MoDMaCAO).

## Nodecellar
In the video below you see a demonstration of the deployment process for the [Nodecellar configuration](nodecellarFloat.extendedTosca).
The OCCI configuration to be deployed consists of 2 virtual machines hosting the nodecellar application that is built with Backbone.js, Twitter Bootstrap, Node.js, Express, and MongoDB. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![Nodecellar](Nodecellar.mp4)


## Configuration management scripts
The exact version of the configuration management scripts used can be found [in this revision](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/7d4dce54de098ccf3267457d94d2b26e60d39c26). The utilized deployment scripts were taken and adjusted from the [alien4cloud](https://github.com/alien4cloud/samples) project.
The up to date version for the individual scripts are enumerated below:

* [Nodecellar](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_nodecellar)
* [Nodejs](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/tosca_nodes_nodejs)
* [Mongod](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/tosca_nodes_mongod)


## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the LAMP configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.04 with Python | 
| User      | ubuntu      | 
| User Data | in configuration |
| SSH Key   | default |
