# Description
In this scenario, a [MongoDB configuration](MongoDB-cluster-withAnsible.occic) is deployed.
Hereby, a special extension for MongoDB application is created using [MoDMaCAO](https://github.com/occiware/MoDMaCAO).

## MongoDB cluster
In the video below you see a demonstration of the deployment process for the [MongoDB cluster configuration](MongoDB-cluster-withAnsible.occic).
The OCCI configuration to be deployed consists of 5 virtual machines hosting, 1 configserver, 1 router, and 3 shards. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![MongoDB Cluster](MongoDB-Cluster-Edited.mp4)

## Configuration management scripts
The configuration management scripts used for this case study are [ansible](https://www.ansible.com/) scripts that can be found here:

* [Configserver](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/configserver)
* [Router](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/router)
* [Shard](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/shard)
* [Cluster](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/cluster)

The exact version of the configuration management scripts used can be found [in this revision](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/ac3e6f5ee3bb7d65984c470dfd428937f7bf4beb).

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the MongoDB configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.04 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |