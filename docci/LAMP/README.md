# Description
In this scenario, a [LAMP configuration](LAMP-cluster-withAnsible.occic) is deployed.
Hereby, a special extension for LAMP stacks is created using [MoDMaCAO](https://github.com/occiware/MoDMaCAO).

## LAMP cluster
In the video below you see a demonstration of the deployment process for the [LAMP cluster configuration](LAMP-cluster-withAnsible.occic).
The OCCI configuration to be deployed consists of 5 virtual machines hosting, 1 apache server, 3 tomcats, and 1 mysql component. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![Lamp Cluster](LAMP-Cluster-Edited.mp4)

## LAMP simple
In this video a simple and small [Lamp stack](LAMP-simple-withAnsible.occic) is deployed and configured.
It consists of only 2 virtual machines. One hosts the apache server and a tomcat and one a mysql server.
![Lamp Simple](LAMPsimple.mp4)

## Configuration management scripts
The exact version of the configuration management scripts used can be found [in this revision](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/ac3e6f5ee3bb7d65984c470dfd428937f7bf4beb).
The up to date version for the individual scripts are enumerated below:

* [Apache Server](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/apacheserver)
* [Tomcat](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/tomcat)
* [MySQL](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/mysql)
* [LAMP](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/lamp/tasks)

## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the LAMP configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Centos7 | 
| User      | centos      | 
| User Data | in configuration      |
| SSH Key   | default |