# Description
In this scenario, a [Multi-Tier configuration](Multi-Tier-Deployment.occic) is deployed.
This use case is taken from the [TOSCA specification 1.2 (Multi-Tier-1 Use Case)](http://docs.oasis-open.org/tosca/TOSCA-Simple-Profile-YAML/v1.2/csd01/TOSCA-Simple-Profile-YAML-v1.2-csd01.html#USE_CASE_MULTI_TIER_1).
Within this stack metrics are gathered via collectd and rsyslog and processed by the ELK stack. Moreover, a Webapplication is deployed.
To realize the deployment configuration management scripts were adapted from [here](https://github.com/openstack/heat-translator/tree/master/translator/tests/data).

This deployment represents a combination of the [ELK use case](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/tree/master/docci/ELK) and [Nodecellar use case](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/tree/master/docci/NodeCellar).

## Multi-Tier-Deployment
In the video below you see a demonstration of the deployment process for the [Multi-Tier configuration](Multi-Tier-Deployment.occic).
The OCCI configuration to be deployed consists of 5 virtual machines hosting, 1 elasticsearch server, 1 logstash server, 1 kibana server, 1 mongodb and 1 nodejs application. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![Multi-Tier-Deployment](Multi-Tier-Deployment.mp4)

## Multi-Tier-Deployment-TOSCA
In the video below you see a demonstration of the deployment process for the [Multi-Tier-TOSCA configuration](Multi-Tier1-ElasticsearchLogstashKibanaFloat.occic).
This use case results from ann automated transformation from a TOSCA yaml file to an OCCI configuration. The deployment model is very similar to the one above mainly reducing the amount
of collectd and rsyslog components. As this deployment results from an automated transformation, the utilized components describing the used configuration management scripts run under a different name.
In this case, [tosca_nodes_softwarecomponent_kibana](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_softwarecomponent_kibana), [tosca_nodes_softwarecomponent_logstash](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_softwarecomponent_logstash), [tosca_nodes_softwarecomponent_elasticsearch](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_softwarecomponent_elasticsearch), [tosca_nodes_softwarecomponent_rsyslog](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_softwarecomponent_rsyslog) and [tosca_nodes_softwarecomponent_collectd](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_softwarecomponent_collectd).
Still, the OCCI configuration to be deployed consists of 5 virtual machines hosting, 1 elasticsearch server, 1 logstash server, 1 kibana server, 1 mongodb and 1 nodejs application. 
In the video below the described OCCI configuration is provisioned and deployed. Moreover, the deployed application is shortly demonstrated.
Please note that the provisioning and deployment times are speeded up:
![Multi-Tier-Deployment](Multi-Tier-Deployment-TOSCA.mp4)


## Configuration management scripts
The configuration management scripts used can be found here:

* [Kibana](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/kibana)
* [Logstash](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/logstash)
* [Elasticsearch](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/elasticsearch)
* [Nodecellar](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/tree/master/cm/ansible/roles/tosca_nodes_nodecellar)
* [Nodejs](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/tosca_nodes_nodejs)
* [Mongod](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/tosca_nodes_mongod)
* [Collectd](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/collectd)
* [Rsyslog](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cm.scripts/tree/master/cm/ansible/roles/rsyslog)


## Platform dependent model transformation
The following table describes the parameters used for the [platform specific transformation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) performed on the LAMP configuration.

|**Attribute**     | **Value**  | 
| ------------- |-------------|
| Operating System | Ubuntu 18.05 with Python | 
| User      | ubuntu      | 
| User Data | in configuration      |
| SSH Key   | default |
